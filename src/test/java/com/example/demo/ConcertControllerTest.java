package com.example.demo;

import com.example.demo.controller.ConcertController;
import com.example.demo.entity.Concert;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

@SpringBootTest
@AutoConfigureMockMvc
public class ConcertControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ConcertController concertController;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void getAllConcerts() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/concerts"))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));
    }

    @Test
    public void getConcertById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/concerts/{id}", 1L))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Concert 1"));
    }

    @Test
    public void getConcertByIdNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/concerts/{id}", 999L))
               .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void createConcert() throws Exception {
        Concert newConcert = new Concert();
        newConcert.setTitle("New Concert");
        newConcert.setArtist("New Artist");
        newConcert.setDate("2023-12-01");
        newConcert.setDescription("Description for the new concert");

        mockMvc.perform(MockMvcRequestBuilders.post("/concerts")
               .contentType(MediaType.APPLICATION_JSON)
               .content(objectMapper.writeValueAsString(newConcert)))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("New Concert"));
    }

    @Test
    public void updateConcert() throws Exception {
        Concert updatedConcert = new Concert();
        updatedConcert.setTitle("Updated Concert");
        updatedConcert.setArtist("Updated Artist");
        updatedConcert.setDate("2023-12-02");
        updatedConcert.setDescription("Updated description");

        mockMvc.perform(MockMvcRequestBuilders.put("/concerts/{id}", 1L)
               .contentType(MediaType.APPLICATION_JSON)
               .content(objectMapper.writeValueAsString(updatedConcert)))
               .andExpect(MockMvcResultMatchers.status().isOk())
               .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Updated Concert"));
    }

    @Test
    public void deleteConcert() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/concerts/{id}", 1L))
               .andExpect(MockMvcResultMatchers.status().isOk());
    }


}
