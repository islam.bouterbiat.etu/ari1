package com.example.demo.service;

import com.example.demo.entity.Concert;
import com.example.demo.entity.Salle;
import com.example.demo.repo.ConcertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ConcertServiceImpl implements ConcertService {

    private final ConcertRepository concertRepository;

    @Autowired
    public ConcertServiceImpl(ConcertRepository concertRepository) {
        this.concertRepository = concertRepository;
    }

    @Override
    public Concert createConcert(String title, String artist, String date, String description, Salle salle) {
        Concert concert = new Concert();
        concert.setTitle(title);
        concert.setArtist(artist);
        concert.setDate(LocalDate.now().toString());
        concert.setDescription(description);
        concert.setSalle(salle);

        return concertRepository.save(concert);
    }

    @Override
    public Concert getConcertById(Long id) {
        return concertRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Concert not found with id: " + id));
    }

    @Override
    public List<Concert> listConcerts() {
        return concertRepository.findAll();
    }

    @Override
    public boolean deleteConcert(Long id) {
        // Check if the Concert with the given ID exists
        if (concertRepository.existsById(id)) {
            concertRepository.deleteById(id); // Delete the Concert
            return true; // Return true to indicate a successful deletion
        } else {
            return false; // Return false if the Concert with the given ID doesn't exist
        }
    }

    @Override
    public Concert editConcert(Long id, Concert updatedConcert) {
        Concert existingConcert = getConcertById(id);

        // Update the fields of the existing concert with the new values
        existingConcert.setTitle(updatedConcert.getTitle());
        existingConcert.setArtist(updatedConcert.getArtist());
        existingConcert.setDate(updatedConcert.getDate());
        existingConcert.setDescription(updatedConcert.getDescription());
        existingConcert.setSalle(updatedConcert.getSalle());

        // Add more fields as needed

        return concertRepository.save(existingConcert);
    }
}
