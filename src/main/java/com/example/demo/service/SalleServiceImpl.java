package com.example.demo.service;

import com.example.demo.entity.Concert;
import com.example.demo.entity.Salle;
import com.example.demo.repo.SalleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SalleServiceImpl implements SalleService {

    private final SalleRepository salleRepository;

    @Autowired
    public SalleServiceImpl(SalleRepository salleRepository) {
        this.salleRepository = salleRepository;
    }

    @Override
    // @Transactional
    public Salle createSalle(String name, int capacite, boolean reservable, String description, List<Concert> concerts) {
        Salle salle = new Salle();
        salle.setNom(name);
        salle.setCapacite(capacite);
        salle.setReservable(reservable);
        salle.setDescription(description);
        salle.setConcerts(concerts);
        return salleRepository.save(salle);
    }

    @Override
    public Optional<Salle> getSalleById(Long id) {
        return salleRepository.findById(id);
    }

    @Override
    public List<Salle> listSalles() {
        return salleRepository.findAll();
    }

    @Override
    // @Transactional
    public boolean deleteSalle(Long id) {
        // Check if the Salle with the given ID exists
        Optional<Salle> salleOptional = salleRepository.findById(id);
        if (salleOptional.isPresent()) {
            Salle salle = salleOptional.get();
            // Set salle reference to null for associated concerts
            salle.getConcerts().forEach(concert -> concert.setSalle(null));
            salleRepository.deleteById(id); // Delete the Salle
            return true; // Return true to indicate a successful deletion
        } else {
            return false; // Return false if the Salle with the given ID doesn't exist
        }
    }

    @Override
    // @Transactional
    public Salle editSalle(Long salle_id, String nom, int capacite, boolean reservable, String description, List<Concert> concerts) throws NotFoundException {
        Optional<Salle> optionalSalle = salleRepository.findById(salle_id);

        if (optionalSalle.isPresent()) {
            Salle salle = optionalSalle.get();
            salle.setNom(nom);
            salle.setCapacite(capacite);
            salle.setReservable(reservable);
            salle.setDescription(description);
            salle.setConcerts(concerts);
            return salleRepository.save(salle);
        } else {
            throw new NotFoundException();
        }
    }
}
