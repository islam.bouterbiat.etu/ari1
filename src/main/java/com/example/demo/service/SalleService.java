package com.example.demo.service;

import com.example.demo.entity.Concert;
import com.example.demo.entity.Salle;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface SalleService {

    Salle createSalle(String name, int capacite, boolean reservable, String description, List<Concert> concerts);

    Optional<Salle> getSalleById(Long id);

    List<Salle> listSalles();

    boolean deleteSalle(Long id);

    Salle editSalle(Long salle_id, String nom, int capacite, boolean reservable, String description, List<Concert> concerts) throws NotFoundException;
}
