package com.example.demo.service;

import com.example.demo.entity.Concert;
import com.example.demo.entity.Salle;

import java.util.List;

public interface ConcertService {

    Concert createConcert(String title, String artist, String date, String description, Salle salle);

    Concert getConcertById(Long id);

    List<Concert> listConcerts();

    boolean deleteConcert(Long id);

    Concert editConcert(Long id, Concert updatedConcert);
}
