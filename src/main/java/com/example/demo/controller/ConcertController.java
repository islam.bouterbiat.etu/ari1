package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import com.example.demo.entity.Concert;

import com.example.demo.service.ConcertService;


import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin("*")
@RequestMapping("/concerts")
public class ConcertController {

    // private final ConcertRepository concertRepository;    
    private final ConcertService concertService;


    @Autowired
        public ConcertController(ConcertService concertService) {
            this.concertService = concertService;
    }


    @GetMapping
    public List<Concert> listConcerts() {
        return concertService.listConcerts();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Concert> obtenirConcertParId(@PathVariable Long id) {
        Concert concert = concertService.getConcertById(id);

        if (concert != null) {
            return ResponseEntity.ok(concert);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("create")
    public Concert createConcert(@RequestBody Concert concertRequest) throws NotFoundException {
        Concert c = concertService.createConcert(concertRequest.getTitle(), concertRequest.getArtist(),
                concertRequest.getDate(), concertRequest.getDescription(),
                concertRequest.getSalle());
        return c;
    }

    @GetMapping("delete/{id}")
    public ResponseEntity<String> deleteConcert(@PathVariable Long id) {
        boolean deleted = concertService.deleteConcert(id);
        if (deleted) {
            return ResponseEntity.ok("Concert with ID " + id + " has been deleted.");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Concert> editConcert(@PathVariable Long id, @RequestBody Concert updatedConcert) {
        try {
            Concert editedConcert = concertService.editConcert(id, updatedConcert);
            return ResponseEntity.ok(editedConcert);
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
