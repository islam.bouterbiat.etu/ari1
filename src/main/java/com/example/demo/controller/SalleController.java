package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.entity.Salle;

import com.example.demo.service.SalleService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/salles")
public class SalleController {

    // private final ConcertRepository concertRepository;
    private final SalleService salleService;

    @Autowired
    public SalleController(SalleService salleService) {
        this.salleService = salleService;
    }

    @GetMapping
    public List<Salle> listSalles() {
        return salleService.listSalles();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Salle> obtenirSalleParId(@PathVariable Long id) {
        Optional<Salle> salle = salleService.getSalleById(id);
        if (salle.isPresent()) {
            return ResponseEntity.ok(salle.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("create")
    public Salle createSalle(@RequestBody Salle salleRequest) throws NotFoundException {
        Salle s = salleService.createSalle(salleRequest.getNom(), salleRequest.getCapacite(),
                salleRequest.isReservable(), salleRequest.getDescription(),
                salleRequest.getConcerts());
        return s;
    }

    @GetMapping("delete/{id}")
    public ResponseEntity<String> deleteSalle(@PathVariable Long id) {
        boolean deleted = salleService.deleteSalle(id);
        if (deleted) {
            return ResponseEntity.ok("Salle with ID " + id + " has been deleted.");
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Salle> editSalle(
            @PathVariable Long id,
            @RequestBody Salle salleEditRequest) throws NotFoundException {
        Salle editedSalle = salleService.editSalle(
                id,
                salleEditRequest.getNom(),
                salleEditRequest.getCapacite(),
                salleEditRequest.isReservable(),
                salleEditRequest.getDescription(),
                salleEditRequest.getConcerts());

        return new ResponseEntity<>(editedSalle, HttpStatus.OK);
    }
}
