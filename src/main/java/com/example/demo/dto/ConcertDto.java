package com.example.demo.dto;

import com.example.demo.entity.Salle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConcertDTO {

    private String title;
    private String artist;
    private String date;
    private String description;
    private Salle salle;
}
