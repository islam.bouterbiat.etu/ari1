package com.example.demo.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SalleDTO {

    private String nom;
    private int capacite;
    private boolean reservable;
    private String description;
    private List<ConcertDTO> concerts;

}
