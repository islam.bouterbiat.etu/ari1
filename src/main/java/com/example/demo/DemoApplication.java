package com.example.demo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.demo.entity.Concert;
import com.example.demo.entity.Salle;
import com.example.demo.repo.ConcertRepository;
import com.example.demo.repo.SalleRepository;
import com.example.demo.service.SalleService;
import com.example.demo.service.ConcertService;



@SpringBootApplication
public class DemoApplication {


	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
public CommandLineRunner demo(ConcertRepository concertRepository, SalleRepository salleRepository, SalleService salleservice, ConcertService concertservice) {
    return (args) -> {
        // Create and save a Salle entity
        Salle salle = new Salle();
        salle.setNom("Salle 1");
        salle.setCapacite(98);
        salle.setReservable(true);
        salle.setDescription("Description de la salle 1");
        salle = salleRepository.save(salle);

        // Get the list of concerts
        List<Concert> concerts = concertservice.listConcerts();

        // Save concerts with the created Salle entity
        concertRepository.save(new Concert(1, "Concert Title 1", "IslamB", "10/10/2015", "Description 1", salle));
        concertRepository.save(new Concert(2, "Concert Title 2", "DahbiaG", "11/10/2015", "Description 1", salle));
        concertRepository.save(new Concert(3, "Concert Title 3", "MenaourT", "12/10/2015", "Description 1", salle));
        concertRepository.save(new Concert(4, "Concert Title 4", "AbdelillahL", "13/10/2015", "Description 1", salle));

        // fetch all concerts
        log.info("Concerts found with findAll():");
        log.info("-------------------------------");
        for (Concert concert : concertRepository.findAll()) {
            log.info(concert.toString());
        }
        log.info("");
    };
}



}
