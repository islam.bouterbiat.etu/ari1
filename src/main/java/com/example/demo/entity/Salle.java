package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Salle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long salle_id;
    private String nom;
    private int capacite;    
    private boolean reservable;    
    private String description;

    // @JsonIgnore
    @OneToMany(mappedBy = "salle" , fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Concert> concerts;

    
}