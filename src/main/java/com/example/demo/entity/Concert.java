package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Concert {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int concert_id;
    private String title;
    private String artist;
    private String date;    
    private String description;
    
    @ManyToOne(optional = true)
    @JsonIgnoreProperties("concerts")
    @JoinColumn(name = "salle_id", nullable = true)
    private Salle salle;

    
}
