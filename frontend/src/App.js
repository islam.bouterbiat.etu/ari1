import logo from './logo.svg';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import './App.css';
import { Layout } from './pages/Layout';
import { Concert } from './pages/concert/Concert';
import { CreateConcert } from "./pages/concert/CreateConcert";
import { EditConcert } from "./pages/concert/EditConcert";
import { ConcertSalle } from "./pages/salle/ConcertSalle";
import { CreateSalle } from "./pages/salle/CreateSalle";
import { EditSalle } from "./pages/salle/EditSalle";
import NoPage from './pages/NoPage';
import { ConcertDetails } from './components/ConcertDetails';

function App() {
  return (
      <BrowserRouter>
          <Routes>
              <Route path="/" element={<Layout />}>
                  <Route index element={<Concert />} />
                  <Route path="/concert/:id" element={<ConcertDetails />} />
                  <Route path="concertsalle" element={<ConcertSalle />} />
                  <Route path="createconcert" element={<CreateConcert />} />
                  <Route path="createsalle" element={<CreateSalle />} />
                  <Route path="editsalle/:id" element={<EditSalle />} />
                  <Route path="editconcert/:id" element={<EditConcert />} />
                  <Route path="*" element={<NoPage />} />
              </Route>
          </Routes>
      </BrowserRouter>
  );
}

export default App;
