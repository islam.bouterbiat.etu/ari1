// @flow 
import * as React from 'react';
import { Link } from 'react-router-dom';


export const NavBar = () => {
    return (
        <nav className="flex flex-row bg-black text-white fixed w-full z-20 top-0 left-0 text-2xl font-semibold max-w-screen-full flex flex-wrap items-center justify-around mx-auto p-4 ">
            <div className="hover:scale-110">
                <Link to="/">Concerts</Link>
            </div>
            <div className="hover:scale-110">
                <Link to="/concertsalle">Salle des concerts</Link>
            </div>
        </nav>
    );
};