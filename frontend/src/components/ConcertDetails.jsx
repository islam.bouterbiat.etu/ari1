
import {Link, useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";

export const ConcertDetails = () => {
    const { id } = useParams();
    const [concerts, setConcerts] = useState([]);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        setLoading(true);
        fetch("http://localhost:8000/concerts")
        .then((response) => response.json())
        .then((data) => {
            setConcerts(data);
            setLoading(false);
        });
    }, []);
    
    const concert = concerts.find((c) => c.concert_id === parseInt(id, 10));
    if (!concert) {
        return <div>Concert non trouvé</div>;
    }
    

    return (
        <div className="px-8">
            <Link to="/">
                <button className="mt-2 relative inline-flex items-center justify-center p-0.5 mb-2 mr-2 overflow-hidden text-sm font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-cyan-500 to-blue-500 group-hover:from-cyan-500 group-hover:to-blue-500 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-cyan-200 dark:focus:ring-cyan-800">
                    <span className="relative px-5 py-1 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
                        <div className="flex">
                        <svg
                            className="w-5 mr-2"
                            fill="currentColor"
                            viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                fill-rule="evenodd"
                                d="M7.707 14.707a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l2.293 2.293a1 1 0 010 1.414z"
                                clip-rule="evenodd"
                            ></path>
                        </svg>
                        Go Back
                        </div>
                    </span>
                </button>
            </Link>
            <h2>Détails du Concert</h2>
            <p>Title: {concert.title}</p>
            <p>Artiste: {concert.artist}</p>
            <p>Salle: {concert.salle?.nom}</p>
            <p>Date: {concert.date}</p>
            <p>Détails: {concert.description}</p>
        </div>
    );
};