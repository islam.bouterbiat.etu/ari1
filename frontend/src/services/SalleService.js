const API_URL = "http://localhost:8000";

export const getAllSalles = async () => {
    const response = await fetch(`${API_URL}/salles`);
    if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }

    return response.json();
};

export const deleteSalle = async (salleId) => {
    const response = await fetch(`${API_URL}/salles/delete/${salleId}`, {
        method: "GET",
    });

    if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }
};
