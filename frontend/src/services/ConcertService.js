const API_URL = "http://localhost:8000";

export const getAllConcerts = async () => {
    const response = await fetch(`${API_URL}/concerts`);
    if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }

    return response.json();
};

export const deleteConcert = async (concertId) => {
    const response = await fetch(`${API_URL}/concerts/delete/${concertId}`, {
        method: "GET",
    });

    if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }
};
