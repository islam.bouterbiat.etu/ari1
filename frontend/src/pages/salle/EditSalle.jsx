import { useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";

export const EditSalle = () => {

    const { id } = useParams();

    const [nom, setNom] = useState("");
    const [capacite, setCapacite] = useState("");
    const [reservable, setReservable] = useState(false);
    const [concerts, setConcerts] = useState([]);
    const [selectedConcerts, setSelectedConcerts] = useState([]);
    const [description, setDescription] = useState("");
    const [notification, setNotification] = useState(null);

    useEffect(() => {
        const fetchSalle = async () => {
            try {
                const response = await fetch(
                    `http://localhost:8000/salles/${id}`
                );
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                const data = await response.json();
                setNom(data.nom);
                setCapacite(data.capacite);
                setReservable(data.reservable);
                setDescription(data.description);
                setSelectedConcerts(data.concerts);
            } catch (error) {
                console.error("Error fetching salle:", error);
            }
        };
        
        const fetchConcerts = async () => {
            try {
                const response = await fetch("http://localhost:8000/concerts");
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                const data = await response.json();
                setConcerts(data);
            } catch (error) {
                console.error("Error fetching concerts:", error);
            }
        };

        fetchSalle();
        fetchConcerts();
    }, [id]);

    const handleSelectChange = (e) => {
        const selectedConcertId = e.target.value;
        const selectedConcert = concerts.find(
            (concert) => concert.concert_id === Number(selectedConcertId)
        );

        if (selectedConcert) {
            setSelectedConcerts([...selectedConcerts, selectedConcert]);
        }
    };

    const removeSelectedConcert = (selectedConcert) => {
        const updatedSelectedConcerts = selectedConcerts.filter(
            (concert) => concert.concert_id !== selectedConcert.concert_id
        );
        setSelectedConcerts(updatedSelectedConcerts);
    };
console.log(selectedConcerts)
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch(
                `http://localhost:8000/salles/edit/${id}`,
                {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        nom,
                        capacite,
                        reservable,
                        description,
                        concerts: selectedConcerts,
                    }),
                }
            );

            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }

            setNotification("Salle updated successfully!");
        } catch (error) {
            console.error("Error updating salle:", error);
            setNotification("Error updating salle");
        }
    };

    return (
        <div className="max-w-md mx-auto my-8 p-6 bg-white rounded-md shadow-md">
            <h1 className="text-2xl font-bold mb-4">Edit Salle</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="nom"
                    >
                        Nom:
                    </label>
                    <input
                        className="w-full p-2 border rounded-md"
                        type="text"
                        id="nom"
                        value={nom}
                        onChange={(e) => setNom(e.target.value)}
                    />
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="capacite"
                    >
                        Capacité:
                    </label>
                    <input
                        className="w-full p-2 border rounded-md"
                        type="number"
                        id="capacite"
                        value={capacite}
                        onChange={(e) => setCapacite(e.target.value)}
                    />
                </div>
                <div className="mb-4">
                    <label className="block text-gray-700 text-sm font-bold mb-2">
                        Reservable:
                        <input
                            className="ml-2"
                            type="checkbox"
                            checked={reservable}
                            onChange={() => setReservable(!reservable)}
                        />
                    </label>
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="concert"
                    >
                        Sélectionnez des concerts:
                    </label>
                    <select
                        className="w-full p-2 border rounded-md"
                        id="concert"
                        value=""
                        onChange={handleSelectChange}
                    >
                        <option value="" disabled hidden>
                            Choisissez un concert
                        </option>
                        {concerts.map((concert) => (
                            <option
                                key={concert.concert_id}
                                value={concert.concert_id}
                            >
                                {concert.title} - {concert.artist} (
                                {concert.date})
                            </option>
                        ))}
                    </select>
                </div>
                <div className="mb-4">
                    <label className="block text-gray-700 text-sm font-bold mb-2">
                        Concerts sélectionnés:
                    </label>
                    {selectedConcerts.length === 0 ? (
                        <p className="text-red-500">
                            Aucun concert sélectionné
                        </p>
                    ) : (
                        <ul>
                            {selectedConcerts.map((selectedConcert) => (
                                <li key={selectedConcert.concert_id}>
                                    {selectedConcert.title} -{" "}
                                    {selectedConcert.artist} (
                                    {selectedConcert.date})
                                    <button
                                        type="button"
                                        onClick={() =>
                                            removeSelectedConcert(
                                                selectedConcert
                                            )
                                        }
                                        className="ml-2 text-red-500"
                                    >
                                        Retirer
                                    </button>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="description"
                    >
                        Description:
                    </label>
                    <textarea
                        className="w-full p-2 border rounded-md"
                        id="description"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    ></textarea>
                </div>
                <button
                    className="bg-blue-500 text-white p-2 rounded-md hover:bg-blue-700"
                    type="submit"
                >
                    Update Salle
                </button>
                {notification && (
                    <div className="mt-4 bg-green-100 border border-green-400 text-green-700 px-4 py-2 rounded-md mb-4">
                        {notification}
                    </div>
                )}
            </form>
        </div>
    );
};
