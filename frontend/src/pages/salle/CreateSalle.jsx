import React, { useState, useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";

export const CreateSalle = () => {
    const [concerts, setConcerts] = useState([]);
    const [selectedConcerts, setSelectedConcerts] = useState([]);
    const [notification, setNotification] = useState(null);

    useEffect(() => {
        const fetchConcerts = async () => {
            try {
                const response = await fetch("http://localhost:8000/concerts");
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                const data = await response.json();
                setConcerts(data);
            } catch (error) {
                console.error("Error fetching concerts:", error);
            }
        };

        fetchConcerts();
    }, []);

    const handleSelectChange = (e) => {
        const selectedId = e.target.value;
        const selectedConcert = concerts.find(
            (concert) => concert.concert_id.toString() === selectedId
        );

        if (
            !selectedConcert ||
            selectedConcertIsAlreadySelected(selectedConcert)
        ) {
            // If the concert is not found or already selected, do nothing
            return;
        }

        setSelectedConcerts((prevSelectedConcerts) => [
            ...prevSelectedConcerts,
            selectedConcert,
        ]);
    };

    const selectedConcertIsAlreadySelected = (selectedConcert) => {
        return (
            selectedConcert &&
            selectedConcerts.some(
                (concert) => concert.concert_id === selectedConcert.concert_id
            )
        );
    };

    const removeSelectedConcert = (selectedConcert) => {
        setSelectedConcerts((prevSelectedConcerts) =>
            prevSelectedConcerts.filter(
                (concert) => concert.concert_id !== selectedConcert.concert_id
            )
        );
    };

    const validationSchema = Yup.object({
        nom: Yup.string().required("Le nom de la salle est requis"),
        capacite: Yup.number()
            .required("La capacité de la salle est requise")
            .positive("La capacité doit être positive")
            .integer("La capacité doit être un nombre entier"),
        description: Yup.string().required(
            "La description de la salle est requise"
        ),
    });

    const formik = useFormik({
        initialValues: {
            nom: "",
            capacite: "",
            reservable: false,
            description: "",
        },
        validationSchema,
        onSubmit: async (values) => {
            try {
                const response = await fetch(
                    "http://localhost:8000/salles/create",
                    {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            ...values,
                            capacite: parseInt(values.capacite, 10),
                            reservable: values.reservable,
                            concerts: selectedConcerts,
                        }),
                    }
                );

                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                const data = await response.json();
                console.log("Salle créée avec succès:", data);

                // Show notification
                setNotification("Salle créée avec succès");

                // Clear form fields
                formik.resetForm();
                setSelectedConcerts([]);
            } catch (error) {
                console.error("Erreur lors de la création de la salle:", error);
            }
        },
    });

    return (
        <div className="max-w-md mx-auto my-8 p-6 bg-white rounded-md shadow-md">
            <h1 className="text-2xl font-bold mb-4">Créer une Salle</h1>
            <form onSubmit={formik.handleSubmit}>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="nom"
                    >
                        Nom:
                    </label>
                    <input
                        className={`w-full p-2 border rounded-md ${
                            formik.touched.nom && formik.errors.nom
                                ? "border-red-500"
                                : ""
                        }`}
                        type="text"
                        id="nom"
                        {...formik.getFieldProps("nom")}
                    />
                    {formik.touched.nom && formik.errors.nom && (
                        <p className="text-red-500 text-sm mt-1">
                            {formik.errors.nom}
                        </p>
                    )}
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="capacite"
                    >
                        Capacité:
                    </label>
                    <input
                        className={`w-full p-2 border rounded-md ${
                            formik.touched.capacite && formik.errors.capacite
                                ? "border-red-500"
                                : ""
                        }`}
                        type="number"
                        id="capacite"
                        {...formik.getFieldProps("capacite")}
                    />
                    {formik.touched.capacite && formik.errors.capacite && (
                        <p className="text-red-500 text-sm mt-1">
                            {formik.errors.capacite}
                        </p>
                    )}
                </div>
                <div className="mb-4">
                    <label className="block text-gray-700 text-sm font-bold mb-2">
                        Reservable:
                        <input
                            className="ml-2"
                            type="checkbox"
                            checked={formik.values.reservable}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            name="reservable"
                        />
                    </label>
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="concert"
                    >
                        Sélectionnez des concerts:
                    </label>
                    <select
                        className={`w-full p-2 border rounded-md ${
                            formik.touched.concerts && formik.errors.concerts
                                ? "border-red-500"
                                : ""
                        }`}
                        id="concert"
                        value=""
                        onChange={handleSelectChange}
                    >
                        <option value="" disabled hidden>
                            Choisissez un concert
                        </option>
                        {concerts.map((concert) => (
                            <option
                                key={concert.concert_id}
                                value={concert.concert_id}
                            >
                                {concert.title} - {concert.artist} (
                                {concert.date})
                            </option>
                        ))}
                    </select>
                    {formik.touched.concerts && formik.errors.concerts && (
                        <p className="text-red-500 text-sm mt-1">
                            {formik.errors.concerts}
                        </p>
                    )}
                </div>
                <div className="mb-4">
                    <label className="block text-gray-700 text-sm font-bold mb-2">
                        Concerts sélectionnés:
                    </label>
                    {selectedConcerts.length === 0 ? (
                        <p className="text-red-500">
                            Aucun concert sélectionné
                        </p>
                    ) : (
                        <ul>
                            {selectedConcerts.map((selectedConcert) => (
                                <li key={selectedConcert.concert_id}>
                                    {selectedConcert.title} -{" "}
                                    {selectedConcert.artist} (
                                    {selectedConcert.date})
                                    <button
                                        type="button"
                                        onClick={() =>
                                            removeSelectedConcert(
                                                selectedConcert
                                            )
                                        }
                                        className="ml-2 text-red-500"
                                    >
                                        Retirer
                                    </button>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="description"
                    >
                        Description:
                    </label>
                    <textarea
                        className={`w-full p-2 border rounded-md ${
                            formik.touched.description &&
                            formik.errors.description
                                ? "border-red-500"
                                : ""
                        }`}
                        id="description"
                        {...formik.getFieldProps("description")}
                    ></textarea>
                    {formik.touched.description &&
                        formik.errors.description && (
                            <p className="text-red-500 text-sm mt-1">
                                {formik.errors.description}
                            </p>
                        )}
                </div>
                <button
                    className="bg-blue-500 text-white p-2 rounded-md hover:bg-blue-700"
                    type="submit"
                >
                    Créer Salle
                </button>
                {notification && (
                    <div className="mt-4 bg-green-100 border border-green-400 text-green-700 px-4 py-2 rounded-md mb-4">
                        {notification}
                    </div>
                )}
            </form>
        </div>
    );
};
