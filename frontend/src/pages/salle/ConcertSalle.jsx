import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { getAllSalles, deleteSalle } from "../../services/SalleService";

export const ConcertSalle = () => {
    const [selectedSalle, setSelectedSalle] = useState(null);
    const [salles, setSalles] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        getAllSalles()
            .then((data) => {
                setSalles(data);
                setLoading(false);
            })
            .catch((error) => {
                console.error("Error fetching salles:", error);
                setLoading(false);
            });
    }, []);

    const handleSelectSalle = (salle) => {
        setSelectedSalle(salle);
    };

    const handleDelete = (salle) => {
        deleteSalle(salle.salle_id)
            .then(() => {

                const updatedSalles = salles.filter(
                    (s) => s.salle_id !== salle.salle_id
                );
                setSalles(updatedSalles);
            })
            .catch((error) => {

                console.log("Failed to delete salle.", error);
            });
    };

    return (
        <div className="px-8 py-6">
            <div className="flex gap-4 items-center">
                <h1 className="text-2xl mb-4 font-bold">Liste des Salles</h1>
                <Link to="/createsalle">
                    <button
                        type="button"
                        className="flex items-center text-white bg-blue-500 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-md text-sm px-5 py-0.5 text-center mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                        <p className="text-xl mr-2">+</p>
                        <p>Create</p>
                    </button>
                </Link>
            </div>
            <ul className="grid grid-cols-4 gap-x-4 gap-y-16">
                {salles.map((salle) => (
                    <li className="basis-1/4" key={salle.salle_id}>
                        <div className="max-w-xs p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                            <a href="#">
                                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                    {salle.nom}
                                </h5>
                            </a>
                            <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                                capacité: {salle.capacite}
                            </p>
                            <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                                {salle.description}
                            </p>
                            <h2>Concerts réservés dans cette salle :</h2>
                            {salle.concerts.length > 0 ? (
                                <ul className="text-gray-900">
                                    {salle.concerts.map((c) => (
                                        <li key={c.id}>
                                            <span className="circle">
                                                &#8226;
                                            </span>{" "}
                                            {c.title}
                                        </li>
                                    ))}
                                </ul>
                            ) : (
                                <p>Not yet</p>
                            )}
                            <div className="w-full flex justify-between mt-8">
                                <Link to={`/editsalle/${salle.salle_id}`}>
                                    <button
                                        type="button"
                                        className="text-white bg-blue-500 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-md text-sm px-5 py-1 text-center mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                    >
                                        Edit
                                    </button>
                                </Link>
                                <button
                                    type="button"
                                    className="text-white bg-red-500 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-medium rounded-md text-sm px-5 py-1 text-center mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                                    onClick={() => handleDelete(salle)}
                                >
                                    Delete
                                </button>
                            </div>
                        </div>
                    </li>
                ))}
            </ul>
            {selectedSalle && (
                <div className="mt-8">
                    <hr className="h-px my-8 bg-gray-200 border-0 dark:bg-gray-700" />
                    <div className="px-8 mt-2">
                        <h2 className="text-xl font-semibold">
                            Informations sur la salle
                        </h2>
                        <p>{selectedSalle.description}</p>
                    </div>
                </div>
            )}
        </div>
    );
};
