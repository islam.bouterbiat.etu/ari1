import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { getAllConcerts, deleteConcert } from "../../services/ConcertService";

export const Concert = () => {
    const [selectedConcert, setSelectedConcert] = useState(null);
    const [concerts, setConcerts] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        getAllConcerts()
            .then((data) => {
                setConcerts(data);
                setLoading(false);
            })
            .catch((error) => {
                console.error("Error fetching concerts:", error);
                setLoading(false);
            });
    }, []);

    const handleSelectConcert = (concert) => {
        setSelectedConcert(concert);
    };

    const handleDelete = (concert) => {
        deleteConcert(concert.concert_id)
            .then(() => {
                // Deletion was successful
                // You can update your UI by removing the deleted concert from the concerts state.
                const updatedConcerts = concerts.filter(
                    (c) => c.concert_id !== concert.concert_id
                );
                setConcerts(updatedConcerts);
            })
            .catch((error) => {
                // Handle deletion failure
                console.log("Failed to delete concert.", error);
            });
    };
    return (
        <div className="px-8 py-6">
            <div className="flex gap-4 items-center">
                <h1 className="text-2xl mb-4 font-bold">Liste des Concerts</h1>
                <Link to="/createconcert">
                    <button
                        type="button"
                        className="flex items-center text-white bg-blue-500 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-md text-sm px-5 py-0.5 text-center mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                        <p className="text-xl mr-2">+</p>
                        <p>Create</p>
                    </button>
                </Link>
            </div>
            <ul className="grid grid-cols-4 gap-x-4 gap-y-16">
                {concerts.map((concert) => (
                    <li className="basis-1/4" key={concert.concert_id}>
                        <div className="mb-3 max-w-xs bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                            <div className="grid pt-6 px-6">
                                <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                    {concert.title}
                                </h5>
                                <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                    Artist : {concert.artist}
                                </h5>
                                <span>{concert.date}</span>
                                <div className="w-full flex justify-between mt-8">
                                    <Link
                                        to={`/editconcert/${concert.concert_id}`}
                                    >
                                        <button
                                            type="button"
                                            className="text-white bg-green-500 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-md text-sm px-5 py-1 text-center mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                        >
                                            Edit
                                        </button>
                                    </Link>
                                    <Link to={`/concert/${concert.concert_id}`}>
                                        <button
                                            type="button"
                                            className="text-white bg-blue-500 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-md text-sm px-5 py-1 text-center mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                        >
                                            Détails
                                        </button>
                                    </Link>
                                    <button
                                        type="button"
                                        className="text-white bg-red-500 hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 font-medium rounded-md text-sm px-5 py-1 text-center mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                                        onClick={() => handleDelete(concert)}
                                    >
                                        Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>
                ))}
            </ul>
            {selectedConcert && (
                <div className="mt-8">
                    <hr className="h-px my-8 bg-gray-200 border-0 dark:bg-gray-700" />
                    <div className="px-8 mt-2">
                        <h2 className="text-xl font-semibold">
                            Informations sur le Concert
                        </h2>
                        <p>{selectedConcert.details}</p>
                    </div>
                </div>
            )}
        </div>
    );
};
