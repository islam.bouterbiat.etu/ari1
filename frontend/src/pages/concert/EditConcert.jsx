import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

export const EditConcert = () => {
    const { id } = useParams();

    const [title, setTitle] = useState("");
    const [artist, setArtist] = useState("");
    const [date, setDate] = useState("");
    const [description, setDescription] = useState("");
    const [selectedSalle, setSelectedSalle] = useState("");
    const [salles, setSalles] = useState([]);
    const [notification, setNotification] = useState(null);

    useEffect(() => {
        const fetchConcert = async () => {
            try {
                const response = await fetch(
                    `http://localhost:8000/concerts/${id}`
                );
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                const data = await response.json();
                setTitle(data.title);
                setArtist(data.artist);
                setDate(data.date);
                setDescription(data.description);
                setSelectedSalle(data.salle?.salle_id);
            } catch (error) {
                console.error("Error fetching concert:", error);
            }
        };

        const fetchSalles = async () => {
            try {
                const response = await fetch("http://localhost:8000/salles");
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                const data = await response.json();
                setSalles(data);
            } catch (error) {
                console.error("Error fetching salles:", error);
            }
        };

        fetchConcert();
        fetchSalles();
    }, [id]);

    const handleSalleChange = (e) => {
        setSelectedSalle(e.target.value);
    };

      const fetchSalleDetails = async (salle_id) => {
          try {
              const response = await fetch(
                  `http://localhost:8000/salles/${salle_id}`
              );
              if (!response.ok) {
                  throw new Error(`HTTP error! Status: ${response.status}`);
              }

              const data = await response.json();
              return data; // Return the salle details
          } catch (error) {
              console.error("Error fetching salle details:", error);
          }
      };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const salleDetails = await fetchSalleDetails(selectedSalle);
        try {
            const response = await fetch(
                `http://localhost:8000/concerts/edit/${id}`,
                {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        title,
                        artist,
                        date,
                        description,
                        salle: salleDetails,
                    }),
                }
            );

            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }

            setNotification("Concert updated successfully!");
        } catch (error) {
            console.error("Error updating concert:", error);
            setNotification("Error updating concert");
        }
    };

    return (
        <div className="max-w-md mx-auto my-8 p-6 bg-white rounded-md shadow-md">
            <h1 className="text-2xl font-bold mb-4">Edit Concert</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="title"
                    >
                        Titre:
                    </label>
                    <input
                        className="w-full p-2 border rounded-md"
                        type="text"
                        id="title"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                    />
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="artist"
                    >
                        Artiste:
                    </label>
                    <input
                        className="w-full p-2 border rounded-md"
                        type="text"
                        id="artist"
                        value={artist}
                        onChange={(e) => setArtist(e.target.value)}
                    />
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="date"
                    >
                        Date:
                    </label>
                    <input
                        className="w-full p-2 border rounded-md"
                        type="date"
                        id="date"
                        value={date}
                        onChange={(e) => setDate(e.target.value)}
                    />
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="description"
                    >
                        Description:
                    </label>
                    <textarea
                        className="w-full p-2 border rounded-md"
                        id="description"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    ></textarea>
                </div>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="salle"
                    >
                        Sélectionnez une salle:
                    </label>
                    <select
                        className="w-full p-2 border rounded-md"
                        id="salle"
                        value={selectedSalle}
                        onChange={handleSalleChange}
                    >
                        <option value="" disabled hidden>
                            Choisissez une salle
                        </option>
                        {salles.map((salle) => (
                            <option key={salle.salle_id} value={salle.salle_id}>
                                {salle.nom}
                            </option>
                        ))}
                    </select>
                </div>
                <button
                    className="bg-blue-500 text-white p-2 rounded-md hover:bg-blue-700"
                    type="submit"
                >
                    Update Concert
                </button>
                {notification && (
                    <div className="mt-4 bg-green-100 border border-green-400 text-green-700 px-4 py-2 rounded-md mb-4">
                        {notification}
                    </div>
                )}
            </form>
        </div>
    );
};
