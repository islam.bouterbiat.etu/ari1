import React, { useState, useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";

export const CreateConcert = () => {
    const [salles, setSalles] = useState([]);
    const [notification, setNotification] = useState(null);

    useEffect(() => {
        const fetchSalles = async () => {
            try {
                const response = await fetch("http://localhost:8000/salles");
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                const data = await response.json();
                setSalles(data);
            } catch (error) {
                console.error("Error fetching salles:", error);
            }
        };

        fetchSalles();
    }, []);

    const validationSchema = Yup.object().shape({
        title: Yup.string().required("Le titre est requis"),
        artist: Yup.string().required("L'artiste est requis"),
        date: Yup.date().required("La date est requise"),
        description: Yup.string().required("La description est requise"),
        selectedSalle: Yup.string().required("Veuillez sélectionner une salle"),
    });

    const formik = useFormik({
        initialValues: {
            title: "",
            artist: "",
            date: "",
            description: "",
            selectedSalle: "",
        },
        validationSchema: validationSchema,
        onSubmit: async (values) => {
            const salleDetails = await fetchSalleDetails(values.selectedSalle);
            try {
                const response = await fetch(
                    "http://localhost:8000/concerts/create",
                    {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            title: values.title,
                            artist: values.artist,
                            date: values.date,
                            description: values.description,
                            salle: salleDetails,
                        }),
                    }
                );

                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }

                const data = await response.json();
                console.log("Concert créé avec succès:", data);

                setNotification("Concert créé avec succès");

                formik.resetForm();
            } catch (error) {
                console.error("Erreur lors de la création du concert:", error);            }
        },
    });

    const handleSalleChange = (e) => {
        formik.handleChange(e);
        formik.setFieldTouched("selectedSalle", true);
    };

    const fetchSalleDetails = async (salle_id) => {
        try {
            const response = await fetch(
                `http://localhost:8000/salles/${salle_id}`
            );
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }

            const data = await response.json();
            return data; // Return the salle details
        } catch (error) {
            console.error("Error fetching salle details:", error);
        }
    };

    return (
        <div className="max-w-md mx-auto my-8 p-6 bg-white rounded-md shadow-md">
            <h1 className="text-2xl font-bold mb-4">Créer un concert</h1>

            <form onSubmit={formik.handleSubmit}>
                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="title"
                    >
                        Titre:
                    </label>
                    <input
                        className={`w-full p-2 border rounded-md ${
                            formik.touched.title && formik.errors.title
                                ? "border-red-500"
                                : ""
                        }`}
                        type="text"
                        id="title"
                        {...formik.getFieldProps("title")}
                    />
                    {formik.touched.title && formik.errors.title && (
                        <div className="text-red-500 text-xs mt-1">
                            {formik.errors.title}
                        </div>
                    )}
                </div>

                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="artist"
                    >
                        Artiste:
                    </label>
                    <input
                        className={`w-full p-2 border rounded-md ${
                            formik.touched.artist && formik.errors.artist
                                ? "border-red-500"
                                : ""
                        }`}
                        type="text"
                        id="artist"
                        {...formik.getFieldProps("artist")}
                    />
                    {formik.touched.artist && formik.errors.artist && (
                        <div className="text-red-500 text-xs mt-1">
                            {formik.errors.artist}
                        </div>
                    )}
                </div>

                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="date"
                    >
                        Date:
                    </label>
                    <input
                        className={`w-full p-2 border rounded-md ${
                            formik.touched.date && formik.errors.date
                                ? "border-red-500"
                                : ""
                        }`}
                        type="date"
                        id="date"
                        {...formik.getFieldProps("date")}
                    />
                    {formik.touched.date && formik.errors.date && (
                        <div className="text-red-500 text-xs mt-1">
                            {formik.errors.date}
                        </div>
                    )}
                </div>

                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="description"
                    >
                        Description:
                    </label>
                    <textarea
                        className={`w-full p-2 border rounded-md ${
                            formik.touched.description &&
                            formik.errors.description
                                ? "border-red-500"
                                : ""
                        }`}
                        id="description"
                        {...formik.getFieldProps("description")}
                    ></textarea>
                    {formik.touched.description &&
                        formik.errors.description && (
                            <div className="text-red-500 text-xs mt-1">
                                {formik.errors.description}
                            </div>
                        )}
                </div>

                <div className="mb-4">
                    <label
                        className="block text-gray-700 text-sm font-bold mb-2"
                        htmlFor="salle"
                    >
                        Sélectionnez une salle:
                    </label>
                    <select
                        className={`w-full p-2 border rounded-md ${
                            formik.touched.selectedSalle &&
                            formik.errors.selectedSalle
                                ? "border-red-500"
                                : ""
                        }`}
                        id="salle"
                        {...formik.getFieldProps("selectedSalle")}
                    >
                        <option value="" disabled hidden>
                            Choisissez une salle
                        </option>
                        {salles.map((salle) => (
                            <option key={salle.salle_id} value={salle.salle_id}>
                                {salle.nom}
                            </option>
                        ))}
                    </select>
                    {formik.touched.selectedSalle &&
                        formik.errors.selectedSalle && (
                            <div className="text-red-500 text-xs mt-1">
                                {formik.errors.selectedSalle}
                            </div>
                        )}
                </div>

                <button
                    className="bg-blue-500 text-white p-2 rounded-md hover:bg-blue-700"
                    type="submit"
                >
                    Créer Concert
                </button>

                {notification && (
                    <div className="mt-4 bg-green-100 border border-green-400 text-green-700 px-4 py-2 rounded-md mb-4">
                        {notification}
                    </div>
                )}
            </form>
        </div>
    );
};
